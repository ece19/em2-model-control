/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: emergencyBreakStateFlow_private.h
 *
 * Code generated for Simulink model 'emergencyBreakStateFlow'.
 *
 * Model version                  : 1.12
 * Simulink Coder version         : 9.6 (R2021b) 14-May-2021
 * C/C++ source code generated on : Wed Jan 19 15:27:23 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_emergencyBreakStateFlow_private_h_
#define RTW_HEADER_emergencyBreakStateFlow_private_h_
#include "rtwtypes.h"

/* Private macros used by the generated code to access rtModel */
#ifndef rtmIsMajorTimeStep
#define rtmIsMajorTimeStep(rtm)        (((rtm)->Timing.simTimeStep) == MAJOR_TIME_STEP)
#endif

#ifndef rtmIsMinorTimeStep
#define rtmIsMinorTimeStep(rtm)        (((rtm)->Timing.simTimeStep) == MINOR_TIME_STEP)
#endif

#ifndef rtmSetTPtr
#define rtmSetTPtr(rtm, val)           ((rtm)->Timing.t = (val))
#endif

/* Imported (extern) block signals */
extern uint16_T Distance;              /* '<Root>/Data Type Conversion' */
extern uint16_T Reset;                 /* '<Root>/Data Type Conversion2' */

#endif                       /* RTW_HEADER_emergencyBreakStateFlow_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
