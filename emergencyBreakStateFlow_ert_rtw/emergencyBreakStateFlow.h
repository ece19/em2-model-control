/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: emergencyBreakStateFlow.h
 *
 * Code generated for Simulink model 'emergencyBreakStateFlow'.
 *
 * Model version                  : 1.12
 * Simulink Coder version         : 9.6 (R2021b) 14-May-2021
 * C/C++ source code generated on : Wed Jan 19 15:27:23 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_emergencyBreakStateFlow_h_
#define RTW_HEADER_emergencyBreakStateFlow_h_
#ifndef emergencyBreakStateFlow_COMMON_INCLUDES_
#define emergencyBreakStateFlow_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                            /* emergencyBreakStateFlow_COMMON_INCLUDES_ */

#include "emergencyBreakStateFlow_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetT
#define rtmGetT(rtm)                   (rtmGetTPtr((rtm))[0])
#endif

#ifndef rtmGetTPtr
#define rtmGetTPtr(rtm)                ((rtm)->Timing.t)
#endif

/* Block states (default storage) for system '<Root>' */
typedef struct {
  uint8_T is_active_c3_emergencyBreakStat;/* '<S1>/Emergency Break' */
  uint8_T is_c3_emergencyBreakStateFlow;/* '<S1>/Emergency Break' */
} DW_emergencyBreakStateFlow_T;

/* Real-time Model Data Structure */
struct tag_RTM_emergencyBreakStateFl_T {
  const char_T *errorStatus;
  RTWSolverInfo solverInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    uint32_T clockTick0;
    time_T stepSize0;
    uint32_T clockTick1;
    SimTimeStep simTimeStep;
    time_T *t;
    time_T tArray[2];
  } Timing;
};

/* Block states (default storage) */
extern DW_emergencyBreakStateFlow_T emergencyBreakStateFlow_DW;

/*
 * Exported Global Signals
 *
 * Note: Exported global signals are block signals with an exported global
 * storage class designation.  Code generation will declare the memory for
 * these signals and export their symbols.
 *
 */
extern uint16_T Counter;               /* '<S1>/Emergency Break' */
extern uint16_T Throttle;              /* '<S1>/Emergency Break' */

/* Model entry point functions */
extern void emergencyBreakStateFlow_initialize(void);
extern void emergencyBreakStateFlow_step(void);
extern void emergencyBreakStateFlow_terminate(void);

/* Real-time Model object */
extern RT_MODEL_emergencyBreakStateF_T *const emergencyBreakStateFlow_M;

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<Root>/Data Type Conversion1' : Unused code path elimination
 * Block '<Root>/Scope' : Unused code path elimination
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'emergencyBreakStateFlow'
 * '<S1>'   : 'emergencyBreakStateFlow/EmergencyBreak'
 * '<S2>'   : 'emergencyBreakStateFlow/EmergencyBreak/Emergency Break'
 */
#endif                               /* RTW_HEADER_emergencyBreakStateFlow_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
