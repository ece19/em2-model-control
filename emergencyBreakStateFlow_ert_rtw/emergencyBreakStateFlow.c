/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: emergencyBreakStateFlow.c
 *
 * Code generated for Simulink model 'emergencyBreakStateFlow'.
 *
 * Model version                  : 1.12
 * Simulink Coder version         : 9.6 (R2021b) 14-May-2021
 * C/C++ source code generated on : Wed Jan 19 15:27:23 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "emergencyBreakStateFlow.h"
#include "emergencyBreakStateFlow_private.h"

/* Named constants for Chart: '<S1>/Emergency Break' */
#define emergencyBreakStateFlo_IN_Drive ((uint8_T)1U)
#define emergencyBreakStateFlow_IN_Stop ((uint8_T)2U)
#define emergencyBreakStateFlow_IN_Wait ((uint8_T)3U)

/* Exported block signals */
uint16_T Counter;                      /* '<S1>/Emergency Break' */
uint16_T Throttle;                     /* '<S1>/Emergency Break' */

/* Block states (default storage) */
DW_emergencyBreakStateFlow_T emergencyBreakStateFlow_DW;

/* Real-time model */
static RT_MODEL_emergencyBreakStateF_T emergencyBreakStateFlow_M_;
RT_MODEL_emergencyBreakStateF_T *const emergencyBreakStateFlow_M =
  &emergencyBreakStateFlow_M_;

/* Model step function */
void emergencyBreakStateFlow_step(void)
{
  uint32_T tmp;

  /* Step: '<Root>/Step' */
  if (emergencyBreakStateFlow_M->Timing.t[0] < 0.01) {
    /* DataTypeConversion: '<Root>/Data Type Conversion' */
    Distance = 300U;
  } else {
    /* DataTypeConversion: '<Root>/Data Type Conversion' */
    Distance = 50U;
  }

  /* End of Step: '<Root>/Step' */

  /* Outputs for Atomic SubSystem: '<Root>/EmergencyBreak' */
  /* Chart: '<S1>/Emergency Break' */
  if (emergencyBreakStateFlow_DW.is_active_c3_emergencyBreakStat == 0U) {
    emergencyBreakStateFlow_DW.is_active_c3_emergencyBreakStat = 1U;
    emergencyBreakStateFlow_DW.is_c3_emergencyBreakStateFlow =
      emergencyBreakStateFlo_IN_Drive;
    Counter = 0U;
    Throttle = 100U;
  } else {
    switch (emergencyBreakStateFlow_DW.is_c3_emergencyBreakStateFlow) {
     case emergencyBreakStateFlo_IN_Drive:
      Throttle = 100U;
      if (Distance >= 2000) {
        emergencyBreakStateFlow_DW.is_c3_emergencyBreakStateFlow =
          emergencyBreakStateFlow_IN_Wait;
        tmp = Counter + 1U;
        if (Counter + 1U > 65535U) {
          tmp = 65535U;
        }

        Counter = (uint16_T)tmp;
        Throttle = 50U;
      }
      break;

     case emergencyBreakStateFlow_IN_Stop:
      Throttle = 0U;
      if (Reset == 1) {
        emergencyBreakStateFlow_DW.is_c3_emergencyBreakStateFlow =
          emergencyBreakStateFlo_IN_Drive;
        Counter = 0U;
        Throttle = 100U;
      }
      break;

     default:
      /* case IN_Wait: */
      Throttle = 50U;
      if (Counter > 10) {
        emergencyBreakStateFlow_DW.is_c3_emergencyBreakStateFlow =
          emergencyBreakStateFlow_IN_Stop;
        Throttle = 0U;
        Counter = 0U;
      } else if (Distance >= 2000) {
        emergencyBreakStateFlow_DW.is_c3_emergencyBreakStateFlow =
          emergencyBreakStateFlow_IN_Wait;
        tmp = Counter + 1U;
        if (Counter + 1U > 65535U) {
          tmp = 65535U;
        }

        Counter = (uint16_T)tmp;
        Throttle = 50U;
      } else if (Distance < 2000) {
        emergencyBreakStateFlow_DW.is_c3_emergencyBreakStateFlow =
          emergencyBreakStateFlo_IN_Drive;
        Counter = 0U;
        Throttle = 100U;
      }
      break;
    }
  }

  /* End of Chart: '<S1>/Emergency Break' */
  /* End of Outputs for SubSystem: '<Root>/EmergencyBreak' */

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  emergencyBreakStateFlow_M->Timing.t[0] =
    ((time_T)(++emergencyBreakStateFlow_M->Timing.clockTick0)) *
    emergencyBreakStateFlow_M->Timing.stepSize0;

  {
    /* Update absolute timer for sample time: [0.001s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The resolution of this integer timer is 0.001, which is the step size
     * of the task. Size of "clockTick1" ensures timer will not overflow during the
     * application lifespan selected.
     */
    emergencyBreakStateFlow_M->Timing.clockTick1++;
  }
}

/* Model initialize function */
void emergencyBreakStateFlow_initialize(void)
{
  /* Registration code */
  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&emergencyBreakStateFlow_M->solverInfo,
                          &emergencyBreakStateFlow_M->Timing.simTimeStep);
    rtsiSetTPtr(&emergencyBreakStateFlow_M->solverInfo, &rtmGetTPtr
                (emergencyBreakStateFlow_M));
    rtsiSetStepSizePtr(&emergencyBreakStateFlow_M->solverInfo,
                       &emergencyBreakStateFlow_M->Timing.stepSize0);
    rtsiSetErrorStatusPtr(&emergencyBreakStateFlow_M->solverInfo,
                          (&rtmGetErrorStatus(emergencyBreakStateFlow_M)));
    rtsiSetRTModelPtr(&emergencyBreakStateFlow_M->solverInfo,
                      emergencyBreakStateFlow_M);
  }

  rtsiSetSimTimeStep(&emergencyBreakStateFlow_M->solverInfo, MAJOR_TIME_STEP);
  rtsiSetSolverName(&emergencyBreakStateFlow_M->solverInfo,"FixedStepDiscrete");
  rtmSetTPtr(emergencyBreakStateFlow_M,
             &emergencyBreakStateFlow_M->Timing.tArray[0]);
  emergencyBreakStateFlow_M->Timing.stepSize0 = 0.001;
}

/* Model terminate function */
void emergencyBreakStateFlow_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
