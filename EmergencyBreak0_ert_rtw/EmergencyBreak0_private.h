/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: EmergencyBreak0_private.h
 *
 * Code generated for Simulink model 'EmergencyBreak0'.
 *
 * Model version                  : 1.12
 * Simulink Coder version         : 9.6 (R2021b) 14-May-2021
 * C/C++ source code generated on : Wed Jan 19 15:42:26 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_EmergencyBreak0_private_h_
#define RTW_HEADER_EmergencyBreak0_private_h_
#include "rtwtypes.h"

/* Imported (extern) block signals */
extern uint16_T Distance;              /* '<Root>/Distance' */
extern uint16_T Reset;                 /* '<Root>/Rest' */

#endif                               /* RTW_HEADER_EmergencyBreak0_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
