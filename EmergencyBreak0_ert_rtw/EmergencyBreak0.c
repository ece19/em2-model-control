/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: EmergencyBreak0.c
 *
 * Code generated for Simulink model 'EmergencyBreak0'.
 *
 * Model version                  : 1.12
 * Simulink Coder version         : 9.6 (R2021b) 14-May-2021
 * C/C++ source code generated on : Wed Jan 19 15:42:26 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "EmergencyBreak0.h"
#include "EmergencyBreak0_private.h"

/* Named constants for Chart: '<S1>/Emergency Break' */
#define EmergencyBreak0_IN_Drive       ((uint8_T)1U)
#define EmergencyBreak0_IN_Stop        ((uint8_T)2U)
#define EmergencyBreak0_IN_Wait        ((uint8_T)3U)

/* Exported block signals */
uint16_T Counter;                      /* '<S1>/Emergency Break' */
uint16_T Throttle;                     /* '<S1>/Emergency Break' */

/* Block states (default storage) */
DW_EmergencyBreak0_T EmergencyBreak0_DW;

/* Real-time model */
static RT_MODEL_EmergencyBreak0_T EmergencyBreak0_M_;
RT_MODEL_EmergencyBreak0_T *const EmergencyBreak0_M = &EmergencyBreak0_M_;

/* Model step function */
void EmergencyBreak0_step(void)
{
  uint32_T tmp;

  /* Outputs for Atomic SubSystem: '<Root>/EmergencyBreak' */
  /* Chart: '<S1>/Emergency Break' incorporates:
   *  Inport: '<Root>/Distance'
   *  Inport: '<Root>/Rest'
   */
  if (EmergencyBreak0_DW.is_active_c3_EmergencyBreak0 == 0U) {
    EmergencyBreak0_DW.is_active_c3_EmergencyBreak0 = 1U;
    EmergencyBreak0_DW.is_c3_EmergencyBreak0 = EmergencyBreak0_IN_Drive;
    Counter = 0U;
    Throttle = 50U;
  } else {
    switch (EmergencyBreak0_DW.is_c3_EmergencyBreak0) {
     case EmergencyBreak0_IN_Drive:
      Throttle = 50U;
      if (Distance >= 2000) {
        EmergencyBreak0_DW.is_c3_EmergencyBreak0 = EmergencyBreak0_IN_Wait;
        tmp = Counter + 1U;
        if (Counter + 1U > 65535U) {
          tmp = 65535U;
        }

        Counter = (uint16_T)tmp;
        Throttle = 30U;
      }
      break;

     case EmergencyBreak0_IN_Stop:
      Throttle = 0U;
      if (Reset == 1) {
        EmergencyBreak0_DW.is_c3_EmergencyBreak0 = EmergencyBreak0_IN_Drive;
        Counter = 0U;
        Throttle = 50U;
      }
      break;

     default:
      /* case IN_Wait: */
      Throttle = 30U;
      if (Counter > 10) {
        EmergencyBreak0_DW.is_c3_EmergencyBreak0 = EmergencyBreak0_IN_Stop;
        Throttle = 0U;
        Counter = 0U;
      } else if (Distance >= 2000) {
        EmergencyBreak0_DW.is_c3_EmergencyBreak0 = EmergencyBreak0_IN_Wait;
        tmp = Counter + 1U;
        if (Counter + 1U > 65535U) {
          tmp = 65535U;
        }

        Counter = (uint16_T)tmp;
        Throttle = 30U;
      } else if (Distance < 2000) {
        EmergencyBreak0_DW.is_c3_EmergencyBreak0 = EmergencyBreak0_IN_Drive;
        Counter = 0U;
        Throttle = 50U;
      }
      break;
    }
  }

  /* End of Chart: '<S1>/Emergency Break' */
  /* End of Outputs for SubSystem: '<Root>/EmergencyBreak' */
}

/* Model initialize function */
void EmergencyBreak0_initialize(void)
{
  /* (no initialization code required) */
}

/* Model terminate function */
void EmergencyBreak0_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
