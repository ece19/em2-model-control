/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: EmergencyBreak0.h
 *
 * Code generated for Simulink model 'EmergencyBreak0'.
 *
 * Model version                  : 1.12
 * Simulink Coder version         : 9.6 (R2021b) 14-May-2021
 * C/C++ source code generated on : Wed Jan 19 15:42:26 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_EmergencyBreak0_h_
#define RTW_HEADER_EmergencyBreak0_h_
#ifndef EmergencyBreak0_COMMON_INCLUDES_
#define EmergencyBreak0_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* EmergencyBreak0_COMMON_INCLUDES_ */

#include "EmergencyBreak0_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

/* Block states (default storage) for system '<Root>' */
typedef struct {
  uint8_T is_active_c3_EmergencyBreak0;/* '<S1>/Emergency Break' */
  uint8_T is_c3_EmergencyBreak0;       /* '<S1>/Emergency Break' */
} DW_EmergencyBreak0_T;

/* Real-time Model Data Structure */
struct tag_RTM_EmergencyBreak0_T {
  const char_T * volatile errorStatus;
};

/* Block states (default storage) */
extern DW_EmergencyBreak0_T EmergencyBreak0_DW;

/*
 * Exported Global Signals
 *
 * Note: Exported global signals are block signals with an exported global
 * storage class designation.  Code generation will declare the memory for
 * these signals and export their symbols.
 *
 */
extern uint16_T Counter;               /* '<S1>/Emergency Break' */
extern uint16_T Throttle;              /* '<S1>/Emergency Break' */

/* Model entry point functions */
extern void EmergencyBreak0_initialize(void);
extern void EmergencyBreak0_step(void);
extern void EmergencyBreak0_terminate(void);

/* Real-time Model object */
extern RT_MODEL_EmergencyBreak0_T *const EmergencyBreak0_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Note that this particular code originates from a subsystem build,
 * and has its own system numbers different from the parent model.
 * Refer to the system hierarchy for this subsystem below, and use the
 * MATLAB hilite_system command to trace the generated code back
 * to the parent model.  For example,
 *
 * hilite_system('emergencyBreakStateFlow/EmergencyBreak')    - opens subsystem emergencyBreakStateFlow/EmergencyBreak
 * hilite_system('emergencyBreakStateFlow/EmergencyBreak/Kp') - opens and selects block Kp
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'emergencyBreakStateFlow'
 * '<S1>'   : 'emergencyBreakStateFlow/EmergencyBreak'
 * '<S2>'   : 'emergencyBreakStateFlow/EmergencyBreak/Emergency Break'
 */
#endif                                 /* RTW_HEADER_EmergencyBreak0_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
